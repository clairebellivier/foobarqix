// Name as you want + convention for _test in the end
// Can't use 2 pkg within 2 different folders (except for _test)
package foobarqix_test

import (
	"testing"

	"gitlab.com/clairedelune/foobarqix"
)

// Test: declare what you want
func Test_multiple_3_replace_by_foo(t *testing.T) {
	result := foobarqix.Foobarqix(3)
	if result != "foo" {
		t.Errorf("Expected to have foo but got %v", result)
	}
}

func Test_not_multiple_3_not_replace(t *testing.T) {
	// Package name + fonction name
	result := foobarqix.Foobarqix(2)
	if result != "2" {
		t.Errorf("Expected to have 2 but got %v", result)
	}
}

// Add "bar" test
func Test_multiple_5_replace_by_bar(t *testing.T) {
	result := foobarqix.Foobarqix(5)
	if result != "bar" {
		t.Errorf("Expected to have bar but got %v", result)
	}
}
