package foobarqix

import "fmt"

func Foobarqix(number int) string {
	if number%3 != 0 && number%5 != 0 {
		return fmt.Sprintf("%d", number)
	} else if number%3 == 0 {
		return "foo"
	} else {
		return "bar"
	}
}
